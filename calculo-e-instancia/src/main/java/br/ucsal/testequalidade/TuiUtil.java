package br.ucsal.testequalidade;

import java.util.Scanner;

public class TuiUtil {

	private static Scanner scanner = new Scanner(System.in);

	public Integer obterNumeroInteiroPositivo() {
		Integer n;
		do {
			System.out.println("Informe um número maior ou igual a zero:");
			n = scanner.nextInt();
			if (n < 0) {
				System.out.println("Número fora da faixa.");
			}
		} while (n < 0);
		return n;
	}

	public void exibirMensagem(String mensagem) {
		System.out.println(mensagem);
	}

}
