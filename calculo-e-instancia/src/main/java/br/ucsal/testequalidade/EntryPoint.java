package br.ucsal.testequalidade;

public class EntryPoint {

	public static void main(String[] args) {
		FatorialHelper fatorialHelper = new FatorialHelper();
		CalculoEHelper calculoEHelper = new CalculoEHelper(fatorialHelper);
		TuiUtil tuiUtil = new TuiUtil();
		CalculoETUI calculoETUI = new CalculoETUI(calculoEHelper, tuiUtil);
		calculoETUI.obterNCalcularExibirE();
	}

}
